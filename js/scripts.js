Vue.component('vue-select', {
    props: ['list'],
    data: function () {
        return {
            value: undefined
        }
    },
    watch: {
        value(newValue){
            this.$emit('input', newValue)
        }
    },
    template: `
        <select v-model="value">
            <option v-for="item of list">{{item}}</option>
        </select>`
})

new Vue({
    el:"#app",
    data: {
        car: undefined,
        cars: ['Audi', 'Aston Martin', 'BMW']
    }
})